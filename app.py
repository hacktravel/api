import logging

import tornado.options
from tornado.options import define, options
from tornado.ioloop import IOLoop
from tornado.web import RequestHandler, Application, url
from controllers.search import SearchHandler

define("port", default=8888, help="run on the given port", type=int)

def make_app():
	return Application([
		url(r"/search", SearchHandler, dict(db=None), name="search")
	], debug=True)

if __name__ == "__main__":
	tornado.options.parse_command_line()
	logging.basicConfig()
	app = make_app()
	app.listen(options.port)
	IOLoop.current().start()
