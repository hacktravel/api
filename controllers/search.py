import json
from tornado.web import RequestHandler

# class Elasticsearch

class SearchHandler(RequestHandler):
	def initialize(self, db=None):
		self.db = db

	def get(self):
		self.set_header("Content-Type", "application/json")
		q = self.get_query_argument('q')
		lat = self.get_query_argument('lat')
		lon = self.get_query_argument('lon')
		max_distance = self.get_query_argument('max_distance')

        # res = Elasticsearch.search()

		res = json.dumps({
			'method': 'get',
			'url': '/search',
			'arguments': {
				'q': q
				'lat': lat,
				'lon': lon,
				'max_distance': max_distance
			}
		})

		self.write(res)
